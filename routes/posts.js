const express = require('express');
const router = express.Router();

const Post = require('../models/postmodel')

router.get('/', async (req, res)=>{
    const getPosts = await Post.find().limit();
    res.json(getPosts)
})



router.post('/', async (req,res)=>{
    const post  =new Post({
        title: req.body.title,
        description: req.body.description
    })
    try{
     const savePost = await post.save()
    res.json(savePost)
}
catch(err){
    res.json(err)
}
})

//GET ANY POSTS BY ID

router.get('/:postId' , async(req,res)=>{
    try{

    const getId = await (Post.findById(req.params.postId));
    res.json(getId);
}
catch(err){
    res.json(err);
}
})

//Delete a post 

router.delete('/:postId', async(req,res)=>{try{
   const removePost = await Post.remove({_id : req.params.postId});
   res.json(removePost)
}
catch(err){
    res.json(err)
}
})


router.patch('/:postId', async (req,res) =>{try{
    const update = await  Post.updateOne({_id:req.params.postId}, {$set : {title: req.body.title, description: req.body.description}})
    res.json(update);
}
catch(err){
    res.status(500).json(err)
}
})





module.exports = router;
